import re
import subprocess
import pandas as pd
import mysql.connector
from iso639 import Lang 
from iso639.exceptions import InvalidLanguageValue
# https://pypi.org/project/iso639-lang/
import plotly.express as px
from config.bdd_config import DB_CONFIG, DB_CONFIG1, DB_CONFIG2, DB_CONFIG3

#--------------------------------------------#
#-------- Définition des fonctions ----------#
#--------------------------------------------#
def executeSQLfile(sql_file_path):
    config_file = 'config/my.cnf'
    try:
        command = f"mysql --defaults-extra-file={config_file} < {sql_file_path}"
        subprocess.run(command, shell=True, check=True)
    except subprocess.CalledProcessError as e:
        print(f"Erreur lors de l'exécution du script SQL: {e}")
                
def dropDatabase(database,DB_CONFIG):
    conn = mysql.connector.connect(**DB_CONFIG)
    cursor = conn.cursor()
    if not database.isalnum():
        raise ValueError("Nom de base de données non valide")
    cursor.execute("SET FOREIGN_KEY_CHECKS=0")
    cursor.execute(f"DROP DATABASE IF EXISTS {database}")
    cursor.execute("SET FOREIGN_KEY_CHECKS=1")
    conn.commit()

def selectAllInTable(name_table: str, DB_CONFIG)->list:
    conn= mysql.connector.connect(**DB_CONFIG)
    cursor= conn.cursor()
    query = f"SELECT * FROM {name_table}"
    cursor.execute(query)
    datas = cursor.fetchall()
    conn.close
    cursor.close
    return datas 

def executeSQLRequest(SQL_resquest: str, datas:list, DB_CONFIG:list)->list:
    conn= mysql.connector.connect(**DB_CONFIG)
    cursor= conn.cursor()
    
    for data in datas: 
        if isinstance(data, int):
            data = (data,)
        cursor.execute(SQL_resquest, tuple(data))
    conn.commit()
    conn.close
    cursor.close

def normalizCountry(country):
    if country == "USA":
        country = 'United States'
    elif country == 'UK':
        country = 'United Kingdom'
    return country.strip()

def selectCountry(country: str, DB_CONFIG )-> str:
    conn= mysql.connector.connect(**DB_CONFIG)
    cursor= conn.cursor()

    search_country = f"%{normalizCountry(country)}%"
    query= """ SELECT
                country.Code
            From
                world.country
            WHERE
                Name LIKE %s
            """
    cursor.execute(query, (search_country,))
    country_code = cursor.fetchall()[0][0]
    conn.commit()
    conn.close
    cursor.close
    return country_code

def iso639_1(language):
    if language == "Pilipino":
        language = "Tagalog"
    try:
        lg = Lang(language)
        if lg.pt1:
            return lg.pt1
    except InvalidLanguageValue:
        for word in language.split():
            try:
                lg = Lang(word)
                if lg.pt1:
                    return lg.pt1
            except InvalidLanguageValue:
                continue


def selectLanguage(country: str, DB_CONFIG):
    conn= mysql.connector.connect(**DB_CONFIG)
    cursor= conn.cursor()
    search_country = f"%{normalizCountry(country)}%"
    query = """ SELECT
                    cl.Language
                FROM
                    countrylanguage cl
                JOIN
                    country c ON cl.CountryCode = c.Code
                WHERE
                    c.Name LIKE %s AND cl.isOfficial = 'T' 
                ORDER BY 
                    cl.Percentage DESC
                LIMIT 1;
                """
    cursor.execute(query, (search_country,))
    language = cursor.fetchall()[0][0]
    conn.commit()
    conn.close
    cursor.close
    return iso639_1(language)

#--------------------------------------------------#
#-------- exécution du fichier world.sql ----------#
#--------------------------------------------------#
sql_file_path = 'world.sql'
dropDatabase('world',DB_CONFIG)
executeSQLfile(sql_file_path)

#-------------------------------------------------#
#-------- Création d'une carte du monde ----------#
#-------------------------------------------------#
# conn = mysql.connector.connect(**DB_CONFIG)
# query = "SELECT country.Name, country.Region FROM country;"
# df = pd.read_sql(query, conn)
# conn.close()

# fig = px.choropleth(
#     df,
#     locations='Name',
#     locationmode='country names',
#     color='Region',
#     title='Carte du Monde avec Couleur par Région'
# )

# fig.write_html("image/WorldMap.html")

#----------------------------------------------------------------#
#-------- exécution du fichier mysqlsampledatabase.sql ----------#
#----------------------------------------------------------------#
sql_file_path = 'Ressources/mysqlsampledatabase.sql'
dropDatabase('classicmodels',DB_CONFIG)
executeSQLfile(sql_file_path)

#------------------------------------------------------#
#-------- exécution du fichier migration.sql ----------#
#------------------------------------------------------#
sql_file_path = 'migration.sql'
dropDatabase('classicmodelsV2',DB_CONFIG)
executeSQLfile(sql_file_path)

#------------------------------------------------------------#
#-------- Récuperer donnée de la BDD classicmodels ----------#
#------------------------------------------------------------#

#-------- Table productlines----------#
datas = selectAllInTable("productlines",DB_CONFIG2)
query_insert = """INSERT INTO 
                    productlines (productLine, 
                                textDescription, 
                                htmlDescription, 
                                image)
                VALUES 
                    (%s, %s, %s, %s)"""
executeSQLRequest(query_insert,datas,DB_CONFIG3)

#-------- Table products----------#
datas = selectAllInTable("products",DB_CONFIG2)
query_insert = """INSERT INTO 
                    products (productCode,
                            productName,
                            productLine,
                            productScale,
                            productVendor,
                            productDescription,
                            quantityInStock,
                            buyPrice,
                            MSRP)
                VALUES 
                    (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
executeSQLRequest(query_insert,datas,DB_CONFIG3)
print("products Ok")

#-------- Table offices----------#
datas = selectAllInTable("offices",DB_CONFIG2)
data_list = []
for data in datas: 
    data_temp = list(data)
    data_temp[6] = selectCountry(data[6],DB_CONFIG1)
    data_list.append(data_temp)

query_insert = """INSERT INTO 
                    offices (officeCode,
                            city,
                            phone,
                            addressLine1,
                            addressLine2,
                            state,
                            country,
                            postalCode,
                            territory)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s)"""
executeSQLRequest(query_insert,data_list,DB_CONFIG3)
print("offices Ok")

#-------- Table employees----------#
datas = selectAllInTable("employees",DB_CONFIG2)
query_insert = """INSERT INTO 
                    employees (employeeNumber,
                                lastName,
                                firstName,
                                extension,
                                email,
                                officeCode,
                                reportsTo,
                                jobTitle)
                VALUES 
                    (%s, %s, %s, %s, %s, %s, %s, %s)"""
executeSQLRequest(query_insert,datas,DB_CONFIG3)
print("employees Ok")

#-------- Table customers----------#
datas = selectAllInTable("customers",DB_CONFIG2)
data_list =[]
for data in datas: 
    country = data[10]
    data_temp = list(data)
    if country != "USA":
        data_temp[10] = selectCountry(country.strip(),DB_CONFIG1)
    data_temp.append(selectLanguage(country,DB_CONFIG1))
    data_list.append(data_temp)


query_insert = """INSERT INTO 
                    customers (customerNumber, 
                                customerName, 
                                contactLastName, 
                                contactFirstName, 
                                phone, 
                                addressLine1, 
                                addressLine2, 
                                city, 
                                state, 
                                postalCode, 
                                country, 
                                salesRepEmployeeNumber, 
                                creditLimit, 
                                language) 
                VALUES 
                    (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"""
for datas in data_list:
    if datas[13] is None :
        print(datas[10])
executeSQLRequest(query_insert,data_list,DB_CONFIG3)

print("customers Ok")

#-------- Table payments----------#
datas = selectAllInTable("payments",DB_CONFIG2)
query_insert = """INSERT INTO 
                    payments (customerNumber, 
                                checkNumber, 
                                paymentDate,
                                amount) 
                VALUES 
                    (%s, %s, %s, %s)"""
executeSQLRequest(query_insert, datas, DB_CONFIG3)
print("payments Ok")

#-------- Table orders----------#
datas = selectAllInTable("orders",DB_CONFIG2)
query_insert = """INSERT INTO 
                    orders (orderNumber,
                            orderDate,
                            requiredDate,
                            shippedDate,
                            status,
                            comments,
                            customerNumber) 
                VALUES 
                    (%s, %s, %s, %s, %s, %s, %s)"""
executeSQLRequest(query_insert, datas, DB_CONFIG3)
print("orders Ok")

#-------- Table orderdetails----------#

datas = selectAllInTable("orderdetails",DB_CONFIG2)
query_insert = """INSERT INTO 
                    orderdetails (orderNumber,
                                    productCode,
                                    quantityOrdered,
                                    priceEach,
                                    orderLineNumber)
                VALUES 
                    (%s, %s, %s, %s, %s)"""
executeSQLRequest(query_insert, datas, DB_CONFIG3)
print("orderdetails Ok")