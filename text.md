# Documentation de la Base de Données `world`

![MLD](image/1704802734875.png)

## Table `city`

**Colonnes :**

* `ID` (int) : Clé primaire, identifiant unique pour chaque ville. Auto-incrémentée pour assurer l'unicité.
* `Name` (char(35)) : Nom de la ville. Type `char` choisi pour une longueur fixe de caractères, optimisant l'espace de stockage.
* `CountryCode` (char(3)) : Code du pays, clé étrangère référençant la table `country`. Format standard des codes de pays selon [ISO 3166-1 alpha-3](https://fr.wikipedia.org/wiki/ISO_3166-1).
* `District` (char(20)) : District où se trouve la ville. Longueur fixe pour uniformité.
* `Population` (int) : Population de la ville. Type entier pour représenter les nombres.

**Contraintes :**

* `NOT NULL` sur toutes les colonnes pour assurer la présence de données. Valeurs par défaut vides si non renseignées.
* Clé étrangère (`FOREIGN KEY`) sur `CountryCode` pour l'intégrité référentielle avec la table `country`.

## Table `country`

**Colonnes :**

* `Code` (char(3)) : Clé primaire, code unique du pays.
* `Name` (char(52)) : Nom du pays.
* `Continent` (enum) : Continent sur lequel le pays est situé. Enumération pour restreindre les valeurs.
* `Region` (char(26)) : Région géographique du pays.
* `SurfaceArea` (decimal(10,2)) : Superficie totale du pays en km².
* `IndepYear` (smallint) : Année d'indépendance du pays.
* `Population` (int) : Population totale.
* `LifeExpectancy` (decimal(3,1)) : Espérance de vie moyenne.
* `GNP` (decimal(10,2)) : Produit National Brut en USD.
* `GNPOld` (decimal(10,2)) : Ancien PNB en USD.
* `LocalName` (char(45)) : Nom local du pays.
* `GovernmentForm` (char(45)) : Forme de gouvernement.
* `HeadOfState` (char(60)) : Chef d'État.
* `Capital` (int) : Référence à la table `city` pour la capitale.
* `Code2` (char(2)) : Autre code du pays.

**Contraintes :**

* `NOT NULL` sur la plupart des colonnes pour garantir des données complètes.
* Valeurs par défaut pour certaines colonnes facilitant les insertions.

## Table `countrylanguage`

**Colonnes :**

* `CountryCode` (char(3)) : Clé étrangère vers `country`. Partie de la clé primaire composite.
* `Language` (char(30)) : Langue parlée. Partie de la clé primaire composite.
* `IsOfficial` (enum('T','F')) : Si la langue est officielle dans le pays.
* `Percentage` (decimal(4,1)) : Pourcentage de la population parlant cette langue.

**Contraintes :**

* Clé primaire composite (`CountryCode`, `Language`) pour identifier de manière unique chaque paire pays-langue.
* `NOT NULL` sur toutes les colonnes pour assurer des données complètes.

## Technique

### Types de Données

1. **CHAR (Character)**

   * Stocke des chaînes de caractères de longueur fixe.
   * Lorsqu'une chaîne est plus courte que la longueur définie, des espaces blancs la complètent.
   * Idéal pour des données de longueur constante, comme des codes de pays.
2. **VARCHAR (Variable Character)**

   * Stocke des chaînes de caractères de longueur variable.
   * L'espace occupé est proportionnel à la taille réelle de la chaîne.
   * Convient pour des textes de longueurs variées, comme des descriptions ou des noms de rue.

### Contexte Historique

* Basé sur des données historiques, les PIB et les chefs d'État correspondent à la période entre 2001 et 2002.
* Exemples :
  * États-Unis (USA) : PIB de l'année  **2000** .
  * Japon (JPN) : PIB de l'année  **2002** .
  * Yougoslavie (YUG) : Dissoute dans les années 90, n'existe plus en 2006.
  * Timor oriental (TMP) : Indépendance déclarée en 2002.

## Questions Parti 1

### Liste des Formes de Gouvernement et Nombre de Pays Associés

#### SQL

```sql
SELECT GovernmentForm, COUNT(*) as NumberOfCountries
FROM country
GROUP BY GovernmentForm
ORDER BY NumberOfCountries DESC;
```

#### Résultat

| Forme de Gouvernement                                 | Nombre de Pays |
| ----------------------------------------------------- | -------------- |
| République                                           | 123            |
| Monarchie constitutionnelle                           | 29             |
| République fédérale                                | 14             |
| Territoire dépendant du Royaume-Uni                  | 12             |
| Monarchie                                             | 5              |
| Département d'outre-mer de la France                 | 4              |
| Territoire non métropolitain de la France            | 4              |
| Monarchie constitutionnelle, Fédération             | 4              |
| Territoire de l'Australie                             | 4              |
| République socialiste                                | 3              |
| Territoire non métropolitain de la Nouvelle-Zélande | 3              |
| Territoire des États-Unis                            | 3              |
| Commonwealth des États-Unis                          | 2              |
| Collectivité territoriale de la France               | 2              |
| Territoire non métropolitain des Pays-Bas            | 2              |
| Territoire dépendant de la Norvège                  | 2              |
| Monarchie (Sultanat)                                  | 2              |
| Partie du Danemark                                    | 2              |
| Région administrative spéciale de la Chine          | 2              |
| République islamique                                 | 2              |
| Fédération                                          | 1              |
| État socialiste                                      | 1              |
| Zone autonome                                         | 1              |
| Administré par l'ONU                                 | 1              |
| Territoire dépendant des États-Unis                 | 1              |
| État indépendant de l'Église                       | 1              |
| Monarchie parlementaire                               | 1              |
| Monarchie constitutionnelle (Émirat)                 | 1              |
| Occupé par le Maroc                                  | 1              |
| République populaire                                 | 1              |
| Monarchie (Émirat)                                   | 1              |
| Co-administré                                        | 1              |
| Fédération d'émirats                               | 1              |
| Coprincipauté parlementaire                          | 1              |
| Émirat islamique                                     | 1              |

#### Commentaire

Il y a beaucoup de République

### Pourquoi countrylanguage.IsOfficial utilise un enum et pas un bool ?

#### Commentaire

1. **Compatibilité Historique :** Si la base de données a été conçue il y a longtemps, le type booléen n'était peut-être pas disponible ou couramment utilisé à l'époque. Les anciennes versions de MySQL, par exemple, ne supportaient pas directement le type booléen.
2. **Clarté des Données :** L'`enum('T','F')` est plus explicite que le type booléen. Il est immédiatement clair que 'T' représente "True" (Vrai) et 'F' représente "False" (Faux), ce qui peut être plus lisible pour des personnes qui ne sont pas familières avec le concept de booléen (0 et 1).
3. **Extensibilité Future :** En utilisant un `enum`, il est facile d'ajouter d'autres états ou options sans modifier le type de données. Par exemple, si vous voulez ajouter un état "Inconnu" ou "Non applicable", cela peut être fait sans perturber la structure existante.
4. **Conventions de Conception :** Le choix peut également être une question de convention de conception ou de préférence personnelle de l'architecte de la base de données. Certaines personnes préfèrent utiliser des `enum` pour des raisons de clarté ou de cohérence avec d'autres parties de la base de données.
5. **Contraintes de Valeurs :** Avec un `enum`, la base de données rejette automatiquement les valeurs qui ne sont pas dans l'ensemble spécifié ('T' ou 'F' dans ce cas). Cela renforce l'intégrité des données, tandis qu'avec un booléen, une erreur de saisie pourrait mener à des valeurs non désirées (autre que 0 ou 1).

### D’apres la BDD, combien de personne dans le monde parle anglais ?

#### SQL

```sql
SELECT 
    SUM(EnglishSpeakingPopulation) AS Population_Speak_English
FROM 
    (SELECT 
        (cl.Percentage / 100) * c.Population AS EnglishSpeakingPopulation
     FROM 
        country c
     JOIN
        countrylanguage cl ON c.Code = cl.CountryCode
     WHERE 
        cl.Language = 'English'
    ) AS SubQuery;

```

#### Résultat

| Population Parlant Anglais |
| -------------------------- |
| 347,077,867.3              |

#### Commentaire

Il y en a trop, il faut les convertir au Français.

### Faire la liste des langues avec le nombre de locuteur, de la plus parlée à la moins parlée.

#### SQL

```sql
SELECT
    cl.Language,
    SUM((cl.Percentage / 100) * c.Population) AS TotalPopulationSpeakingLanguage
FROM  
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
GROUP BY 
    cl.Language
ORDER BY 
    TotalPopulationSpeakingLanguage DESC
```

#### Résultat

| Langue     | Population Totale Parlant la Langue |
| ---------- | ----------------------------------- |
| Chinois    | 1,191,843,539                       |
| Hindi      | 405,633,070                         |
| Espagnol   | 355,029,462                         |
| Anglais    | 347,077,867                         |
| Arabe      | 233,839,239                         |
| Bengali    | 209,304,719                         |
| Portugais  | 177,595,269                         |
| Russe      | 160,807,561                         |
| Japonais   | 126,814,108                         |
| Pendjabi   | 104,025,371                         |
| Allemand   | 92,133,585                          |
| Javanais   | 83,570,158                          |
| Télougou  | 79,065,636                          |
| Marathi    | 75,019,094                          |
| Coréen    | 72,291,372                          |
| Vietnamien | 70,616,218                          |
| Français  | 69,980,880                          |
| Tamoul     | 68,691,536                          |
| Ourdou     | 63,589,470                          |
| Turc       | 62,205,657                          |

#### Commentaire

Pour que la langue française puisse dominer le monde, il reste encore beaucoup de travail à accomplir.

### Faire la liste des pays qui ont plus 10 000 000 d’hab. avec leur capitale et le % de la population qui habite dans la capitale.

#### SQL

```sql
SELECT
    c.Name AS CountryName,
    cy.Name AS CapitalName,
    c.Population AS CountryPopulation,
    cy.Population AS CapitalPopulation,
    (cy.Population * 100.0) / c.Population AS PercentagePopulationInCapital
FROM
    country c
JOIN
    city cy ON c.Capital = cy.ID
WHERE
    c.Population >= 10000000
ORDER BY 
    PercentagePopulationInCapital DESC;

```

#### Résultat

| Nom du Pays     | Nom de la Capitale  | Population du Pays | Population de la Capitale | % de la Population dans la Capitale |
| --------------- | ------------------- | ------------------ | ------------------------- | ----------------------------------- |
| Chili           | Santiago de Chile   | 15,211,000         | 4,703,954                 | 30.92%                              |
| Pérou          | Lima                | 25,662,000         | 6,464,693                 | 25.19%                              |
| Corée du Sud   | Séoul              | 46,844,000         | 9,981,619                 | 21.31%                              |
| Cuba            | La Havane           | 11,201,000         | 2,256,000                 | 20.14%                              |
| Irak            | Bagdad              | 23,115,000         | 4,336,000                 | 18.76%                              |
| Hongrie         | Budapest            | 10,043,200         | 1,811,552                 | 18.04%                              |
| Biélorussie    | Minsk               | 10,236,000         | 1,674,000                 | 16.35%                              |
| Angola          | Luanda              | 12,878,000         | 2,022,000                 | 15.70%                              |
| Arabie Saoudite | Riyad               | 21,607,000         | 3,324,000                 | 15.38%                              |
| Colombie        | Santafé de Bogotá | 42,321,000         | 6,260,862                 | 14.79%                              |

#### Liste des 10 pays avec le plus fort taux de croissance entre n et n-1 avec le % de croissance

#### SQL

```sql
SELECT
    c.Name,
    c.GNP,
    c.GNPold,
    ((c.GNP - c.GNPOld) / c.GNPOld) * 100 AS PercentageGrowth
FROM
    country c
WHERE
    c.GNPOld > 0
ORDER BY
    PercentageGrowth DESC
LIMIT 10;

```

#### Résultat

| Nom du Pays                        | PNB Actuel (en millions) | PNB Précédent (en millions) | Croissance du PNB (%) |
| ---------------------------------- | ------------------------ | ----------------------------- | --------------------- |
| République démocratique du Congo | 6,964.00                 | 2,474.00                      | 181.49                |
| Turkménistan                      | 4,397.00                 | 2,000.00                      | 119.85                |
| Tadjikistan                        | 1,990.00                 | 1,056.00                      | 88.45                 |
| Estonie                            | 5,328.00                 | 3,371.00                      | 58.05                 |
| Albanie                            | 3,205.00                 | 2,500.00                      | 28.20                 |
| Suriname                           | 870.00                   | 706.00                        | 23.23                 |
| Iran                               | 195,746.00               | 160,151.00                    | 22.23                 |
| Bulgarie                           | 12,178.00                | 10,169.00                     | 19.76                 |
| Honduras                           | 5,333.00                 | 4,697.00                      | 13.54                 |
| Lettonie                           | 6,398.00                 | 5,639.00                      | 13.46                 |

### Liste des pays plurilingues avec pour chacun le nombre de langues parlées.

#### SQL

```sql
SELECT
    c.Name AS Country,
    COUNT(cl.Language) AS TotalLanguages
FROM
    country c
JOIN
    countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY
    c.Name
HAVING
    TotalLanguages > 1
ORDER BY
    TotalLanguages DESC;

```

#### Résultat

| Pays                               | Nombre Total de Langues |
| ---------------------------------- | ----------------------- |
| Canada                             | 12                      |
| Chine                              | 12                      |
| Inde                               | 12                      |
| Fédération de Russie             | 12                      |
| États-Unis                        | 12                      |
| Tanzanie                           | 11                      |
| Afrique du Sud                     | 11                      |
| République démocratique du Congo | 10                      |
| Iran                               | 10                      |
| Kenya                              | 10                      |

### Liste des pays avec plusieurs langues officielles, le nombre de langues officielle et le nombre de langues du pays.

#### SQL

```sql
SELECT
    c.Name AS Country,
    COUNT(cl.Language) AS TotalLanguages,
    SUM(CASE WHEN cl.IsOfficial = 'T' THEN 1 ELSE 0 END) AS OfficialLanguages
FROM
    country c
JOIN
    countrylanguage cl ON c.Code = cl.CountryCode
GROUP BY
    c.Name
HAVING
    OfficialLanguages > 1
ORDER BY
    OfficialLanguages DESC;

```

#### Résultat

| Pays                    | Nombre Total de Langues | Langues Officielles |
| ----------------------- | ----------------------- | ------------------- |
| Afrique du Sud          | 11                      | 4                   |
| Suisse                  | 4                       | 4                   |
| Luxembourg              | 5                       | 3                   |
| Vanuatu                 | 3                       | 3                   |
| Singapour               | 3                       | 3                   |
| Pérou                  | 3                       | 3                   |
| Bolivie                 | 4                       | 3                   |
| Belgique                | 6                       | 3                   |
| Somalie                 | 2                       | 2                   |
| Nauru                   | 5                       | 2                   |
| Palau                   | 4                       | 2                   |
| Paraguay                | 4                       | 2                   |
| Roumanie                | 6                       | 2                   |
| Rwanda                  | 2                       | 2                   |
| Burundi                 | 3                       | 2                   |
| Malte                   | 2                       | 2                   |
| Seychelles              | 3                       | 2                   |
| Togo                    | 8                       | 2                   |
| Tonga                   | 2                       | 2                   |
| Tuvalu                  | 3                       | 2                   |
| Samoa américaines      | 3                       | 2                   |
| Samoa                   | 3                       | 2                   |
| Antilles néerlandaises | 3                       | 2                   |
| Îles Marshall          | 2                       | 2                   |
| Madagascar              | 2                       | 2                   |
| Afghanistan             | 5                       | 2                   |
| Lesotho                 | 3                       | 2                   |
| Sri Lanka               | 3                       | 2                   |
| Kirghizistan            | 7                       | 2                   |
| Israël                 | 3                       | 2                   |
| Irlande                 | 2                       | 2                   |
| Guam                    | 5                       | 2                   |
| Groenland               | 2                       | 2                   |
| Îles Féroé           | 2                       | 2                   |
| Finlande                | 5                       | 2                   |
| Chypre                  | 2                       | 2                   |
| Biélorussie            | 4                       | 2                   |
| Canada                  | 12                      | 2                   |

### Liste des langues parlées en France avec le % pour chacune.

#### SQL

```sql
SELECT
    cl.Language,
    cl.Percentage
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
WHERE
    c.Name = 'France'
ORDER BY
    Percentage DESC;
```

#### Résultat

| Langue    | Pourcentage (%) |
| --------- | --------------- |
| Français | 93.6            |
| Arabe     | 2.5             |
| Portugais | 1.2             |
| Italien   | 0.4             |
| Espagnol  | 0.4             |
| Turc      | 0.4             |

#### Commentaire

Le Français devrait être la langue unique en France !

### Pareil en chine.

#### SQL

```sql
SELECT
    cl.Language,
    cl.Percentage
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
WHERE
    c.Name = 'China'
ORDER BY
    Percentage DESC;
```

#### Résultat

| Langue    | Pourcentage (%) |
| --------- | --------------- |
| Chinois   | 92.0            |
| Zhuang    | 1.4             |
| Mantšu   | 0.9             |
| Hui       | 0.8             |
| Miao      | 0.7             |
| Uighur    | 0.6             |
| Yi        | 0.6             |
| Tujia     | 0.5             |
| Mongol    | 0.4             |
| Tibétain | 0.4             |
| Dong      | 0.2             |
| Puyi      | 0.2             |

### Pareil aux états unis.

#### SQL

```sql
SELECT
    cl.Language,
    cl.Percentage
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
WHERE
    c.Name = 'United States'
ORDER BY
    Percentage DESC;
```

#### résultat

| Langue     | Pourcentage (%) |
| ---------- | --------------- |
| Anglais    | 86.2            |
| Espagnol   | 7.5             |
| Français  | 0.7             |
| Allemand   | 0.7             |
| Chinois    | 0.6             |
| Italien    | 0.6             |
| Tagalog    | 0.4             |
| Coréen    | 0.3             |
| Polonais   | 0.3             |
| Japonais   | 0.2             |
| Portugais  | 0.2             |
| Vietnamien | 0.2             |

### Pareil aux UK.

#### SQL

```sql
SELECT
    cl.Language,
    cl.Percentage
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
WHERE
    c.Name = 'United Kingdom'
ORDER BY
    Percentage DESC;
```

#### résultat

| Langue  | Pourcentage (%) |
| ------- | --------------- |
| Anglais | 97.3            |
| Kymri   | 0.9             |
| Gaeli   | 0.1             |

### Pour chaque région quelle est la langue la plus parler et quel pourcentage de la population la parle.

#### SQL

```sql
USE world;
DROP TABLE IF EXISTS tmp;
CREATE TABLE tmp(
    Language VARCHAR(50),
    Region VARCHAR(50),
    Pourcent FLOAT
);

INSERT INTO tmp(Language, Region, Pourcent)
SELECT
    LanguageRegion.Language,
    LanguageRegion.Region,
    (LanguageRegion.TotalPopulationSpeakingLanguage*100)/ PopRegion.TotalPopulation AS Pourcent
FROM
    (SELECT
        c.Region,
        SUM(c.Population) AS TotalPopulation
    FROM
        country c
    GROUP BY
        c.Region) AS PopRegion
JOIN
    (SELECT
        cl.Language,
        c.Region,
        SUM((cl.Percentage / 100) * c.Population) AS TotalPopulationSpeakingLanguage
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    GROUP BY
        cl.Language,
        c.Region) AS LanguageRegion
ON PopRegion.Region = LanguageRegion.Region
WHERE PopRegion.TotalPopulation > 0 AND (LanguageRegion.TotalPopulationSpeakingLanguage*100)/ PopRegion.TotalPopulation IS NOT NULL;

SELECT * FROM tmp;
select t.*
from tmp t
left join tmp t2
on t.Region = t2.Region AND t.Pourcent < t2.Pourcent
where t2.Pourcent IS NULL;USE world;
DROP TABLE IF EXISTS tmp;
CREATE TABLE tmp(
    Language VARCHAR(50),
    Region VARCHAR(50),
    Pourcent FLOAT
);

INSERT INTO tmp(Language, Region, Pourcent)
SELECT
    LanguageRegion.Language,
    LanguageRegion.Region,
    (LanguageRegion.TotalPopulationSpeakingLanguage*100)/ PopRegion.TotalPopulation AS Pourcent
FROM
    (SELECT
        c.Region,
        SUM(c.Population) AS TotalPopulation
    FROM
        country c
    GROUP BY
        c.Region) AS PopRegion
JOIN
    (SELECT
        cl.Language,
        c.Region,
        SUM((cl.Percentage / 100) * c.Population) AS TotalPopulationSpeakingLanguage
    FROM
        countrylanguage cl
    JOIN
        country c ON cl.CountryCode = c.Code
    GROUP BY
        cl.Language,
        c.Region) AS LanguageRegion
ON PopRegion.Region = LanguageRegion.Region
WHERE PopRegion.TotalPopulation > 0 AND (LanguageRegion.TotalPopulationSpeakingLanguage*100)/ PopRegion.TotalPopulation IS NOT NULL;

SELECT * FROM tmp;
select t.*
from tmp t
left join tmp t2
on t.Region = t2.Region AND t.Pourcent < t2.Pourcent
where t2.Pourcent IS NULL;
```

#### résultat

| Language         | Region                    | Pourcent |
| ---------------- | ------------------------- | -------- |
| Spanish          | Caribbean                 | 56.4523  |
| Hindi            | Southern and Central Asia | 27.1784  |
| Kongo            | Central Africa            | 12.002   |
| Italian          | Southern Europe           | 37.5353  |
| Arabic           | Middle East               | 47.3264  |
| Portuguese       | South America             | 48.0184  |
| Samoan           | Polynesia                 | 23.238   |
| English          | Australia and New Zealand | 82.1663  |
| German           | Western Europe            | 47.5619  |
| Oromo            | Eastern Africa            | 7.85232  |
| Hausa            | Western Africa            | 13.1841  |
| Russian          | Eastern Europe            | 48.2489  |
| Spanish          | Central America           | 90.3404  |
| English          | North America             | 83.59    |
| Javanese         | Southeast Asia            | 16.1164  |
| Zulu             | Southern Africa           | 20.2804  |
| Chinese          | Eastern Asia              | 77.9975  |
| Swedish          | Nordic Countries          | 34.1596  |
| Arabic           | Northern Africa           | 80.8491  |
| Lithuanian       | Baltic Countries          | 40.295   |
| Papuan Languages | Melanesia                 | 58.5978  |
| Kiribati         | Micronesia                | 15.5129  |
| English          | British Islands           | 97.3655  |

#### Commentaire

Il manque le Français ....

### Est-ce que la somme des pourcentages de langues parlées dans un pays est égale à 100 ? Pourquoi

### SQL

```sql
SELECT 
    c.Name,
    SUM(cl.Percentage) AS SumPercentageLanguage
FROM
    countrylanguage cl
JOIN
    country c ON cl.CountryCode = c.Code
GROUP BY 
    c.Name;

```

### Résultat

| Name                 | SumPercentageLanguage |
| -------------------- | --------------------- |
| Aruba                | 98.9                  |
| Afghanistan          | 96.1                  |
| Angola               | 95.4                  |
| Anguilla             | 0.0                   |
| Albania              | 99.8                  |
| Andorra              | 93.9                  |
| Netherlands Antilles | 94.0                  |
| United Arab Emirates | 42.0                  |
| Argentina            | 98.8                  |
| Armenia              | 96.0                  |

### Commentaire

Non, car dans certains pays, on peux supposer, qu'il y a des langues qui ne sont pas référencées dans la BDD.

### Faire une carte du monde, avec une couleur par région. (chaque pays étant dans la bonne couleur).

### Code

```python
conn = mysql.connector.connect(**DB_CONFIG)
query = "SELECT country.Name, country.Region FROM country;"
df = pd.read_sql(query, conn)
conn.close()

fig = px.choropleth(
    df,
    locations='Name',
    locationmode='country names',
    color='Region',
    title='Carte du Monde avec Couleur par Région'
)

fig.show()
```

### Résultat

[image/WorldMap.html](image/WorldMap.html)

## Question parti 2

### Pour chaque pays (avec au moins 1 client) le nombre de client, la langue officielle du pays (si plusieurs, peu importe laquelle), le CA total.

#### SQL

```sql
select
    country,
    count(distinct c.customerNumber) as `nombre de client`,
    max(language) AS `langue`,
    sum(od.priceEach*quantityOrdered) as `CA`
from customers as c
JOIN orders as o on c.customerNumber = o.customerNumber
JOIN orderdetails as od on od.orderNumber = o.orderNumber 
group by country;
```

### Résultat

| country | nombre de client | langue | CA         |
| ------- | ---------------- | ------ | ---------- |
| AUS     | 5                | en     | 562582.59  |
| AUT     | 2                | de     | 188540.06  |
| BEL     | 2                | nl     | 100068.76  |
| CAN     | 3                | en     | 205911.86  |
| CHE     | 1                | de     | 108777.92  |
| DEU     | 3                | de     | 196470.99  |
| DNK     | 2                | da     | 218994.92  |
| ESP     | 5                | es     | 1099389.09 |
| FIN     | 3                | fi     | 295149.35  |
| FRA     | 12               | fr     | 1007374.02 |
| GBR     | 5                | en     | 436947.44  |
| HKG     | 1                | zh     | 45480.79   |
| IRL     | 1                | en     | 49898.27   |
| ITA     | 4                | it     | 360616.81  |
| JPN     | 2                | ja     | 167909.95  |
| NOR     | 3                | no     | 270846.30  |
| NZL     | 4                | en     | 476847.01  |
| PHL     | 1                | tl     | 87468.30   |
| SGP     | 2                | zh     | 263997.78  |
| SWE     | 2                | sv     | 187638.35  |
| UMI     | 35               | en     | 3273280.05 |

### La liste des 10 clients qui ont rapporté le plus gros CA, avec le CA correspondant.

#### SQL

```sql
SELECT 
    cu.customerName,
    SUM(od.quantityOrdered * od.priceEach) AS TotalRevenue
FROM
    classicmodelsV2.customers cu
JOIN 
    classicmodelsV2.orders o ON cu.customerNumber = o.customerNumber
JOIN 
    classicmodelsV2.orderdetails od ON o.orderNumber = od.orderNumber
GROUP BY 
    cu.customerNumber
ORDER BY 
    TotalRevenue DESC
LIMIT 10;
```

### Résultat

### La durée moyenne (en jours) entre les dates de commandes et les dates d’expédition (de la même commande).

#### SQL

```sql
SELECT
    AVG(DATEDIFF(shippedDate, orderDate))AS `durée moyenne`
FROM 
    orders
WHERE
    shippedDate IS NOT NULL;
```

#### Résultat

| durée moyenne |
| -------------- |
| 3.7564         |

### Les 10 produits les plus vendus.

#### SQL

```sql
SELECT
    od.productCode,
    p.productName,
    SUM(od.quantityOrdered) AS `quantité Total`
FROM
    orderdetails od 
JOIN
    products p ON od.productCode = p.productCode
GROUP BY 
    od.productCode
ORDER BY `quantité Total` DESC
LIMIT 10;
```

### Résultat

| productCode | productName                             | quantité Total |
| ----------- | --------------------------------------- | --------------- |
| S18_3232    | 1992 Ferrari 360 Spider red             | 1808            |
| S18_1342    | 1937 Lincoln Berline                    | 1111            |
| S700_4002   | American Airlines: MD-11S               | 1085            |
| S18_3856    | 1941 Chevrolet Special Deluxe Cabriolet | 1076            |
| S50_1341    | 1930 Buick Marquette Phaeton            | 1074            |
| S18_4600    | 1940s Ford truck                        | 1061            |
| S10_1678    | 1969 Harley Davidson Ultimate Chopper   | 1057            |
| S12_4473    | 1957 Chevy Pickup                       | 1056            |
| S18_2319    | 1964 Mercedes Tour Bus                  | 1053            |
| S24_3856    | 1956 Porsche 356A Coupe                 | 1052            |

### Pour chaque pays le produits le plus vendu.

#### SQL

```sql
SELECT 
    country,
    productCode,
    productName,
    TotalQuantitySold
FROM (
    SELECT 
        cu.country,
        p.productCode,
        p.productName,
        SUM(od.quantityOrdered) AS TotalQuantitySold,
        RANK() OVER (PARTITION BY cu.country ORDER BY SUM(od.quantityOrdered) DESC) AS productRank
    FROM 
        customers cu
    JOIN 
        orders o ON cu.customerNumber = o.customerNumber
    JOIN 
        orderdetails od ON o.orderNumber = od.orderNumber
    JOIN 
        products p ON od.productCode = p.productCode
    GROUP BY 
        cu.country, p.productCode
) AS subquery
WHERE 
    productRank = 1;

```

### Résultat


| country | productCode | productName                                 | TotalQuantitySold |
| ------- | ----------- | ------------------------------------------- | ----------------- |
| AUS     | S18_2949    | 1913 Ford Model T Speedster                 | 231               |
| AUT     | S12_4675    | 1969 Dodge Charger                          | 128               |
| BEL     | S18_3856    | 1941 Chevrolet Special Deluxe Cabriolet     | 95                |
| CAN     | S32_3522    | 1996 Peterbilt 379 Stake Bed with Outrigger | 97                |
| CAN     | S700_2824   | 1982 Camaro Z28                             | 97                |
| CHE     | S18_1889    | 1948 Porsche 356-A Roadster                 | 91                |
| DEU     | S24_3371    | 1971 Alpine Renault 1600s                   | 117               |
| DNK     | S18_3685    | 1948 Porsche Type 356 Roadster              | 101               |
| ESP     | S18_3232    | 1992 Ferrari 360 Spider red                 | 336               |
| FIN     | S18_3232    | 1992 Ferrari 360 Spider red                 | 141               |
| FRA     | S50_4713    | 2002 Yamaha YZR M1                          | 278               |
| GBR     | S24_2887    | 1952 Citroen-15CV                           | 162               |
| HKG     | S24_1785    | 1928 British Royal Navy Airplane            | 79                |
| IRL     | S18_4027    | 1970 Triumph Spitfire                       | 50                |
| ITA     | S24_1785    | 1928 British Royal Navy Airplane            | 162               |
| JPN     | S700_4002   | American Airlines: MD-11S                   | 92                |
| NOR     | S10_1678    | 1969 Harley Davidson Ultimate Chopper       | 89                |
| NZL     | S18_3278    | 1969 Dodge Super Bee                        | 142               |
| PHL     | S18_4721    | 1957 Corvette Convertible                   | 94                |
| SGP     | S18_4600    | 1940s Ford truck                            | 128               |
| SWE     | S18_4600    | 1940s Ford truck                            | 97                |
| UMI     | S12_4473    | 1957 Chevy Pickup                           | 523               |

### Le produit qui a rapporté le plus de bénéfice.

#### SQL

```sql
SELECT
    od.productCode,
    productName,
    SUM(quantityOrdered * (priceEach - buyPrice)) AS bénéfice
FROM
    orderdetails AS od
JOIN
    products AS pr ON od.productCode = pr.productCode
GROUP BY 
    od.productCode
ORDER BY 
    bénéfice DESC
LIMIT 1;
```

### Résultat


| productCode | productName                 | bénéfice |
| ----------- | --------------------------- | ---------- |
| S18_3232    | 1992 Ferrari 360 Spider red | 135996.78  |

### La moyenne des différences entre le MSRP et le prix de vente (en pourcentage).

#### SQL

```sql
select p.productName, avg(p.MSRP - od.priceEach) as avgdif
from products as p
join orderdetails as od on p.productCode = od.productCode
group by p.productCode order by avgdif desc;
```

### Résultat

| productName                          | avgdif    |
| ------------------------------------ | --------- |
| 1968 Ford Mustang                    | 22.123704 |
| 2003 Harley-Davidson Eagle Drag Bike | 21.371786 |
| 2001 Ferrari Enzo                    | 20.703333 |
| 1993 Mazda RX-7                      | 20.234444 |
| 1998 Chrysler Plymouth Prowler       | 19.587143 |
| ...                                  | ...       |
| 1982 Lamborghini Diablo              | 3.817407  |
| 1982 Ducati 996 R                    | 3.635926  |
| 1958 Chevy Corvette Limited Edition  | 3.296429  |
| 1939 Chevrolet Deluxe Coupe          | 3.260000  |

### Pour chaque pays (avec au moins 1 client) le nombre de bureaux dans le même pays.

#### SQL

```sql
select o.country, count(*) AS `nb de bureau`
from offices as o
where (
    select count(*)
    from customers as cc
    where cc.country = o.country
) > 1
group by o.country;
```

### Résultat

| country   | nb de bureau |
| --------- | ------------ |
| USA       | 3            |
| France    | 1            |
| Japan     | 1            |
| Australia | 1            |
| UK        | 1            |
